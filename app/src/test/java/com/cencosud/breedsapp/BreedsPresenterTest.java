package com.cencosud.breedsapp;

import com.cencosud.breedsapp.mvp.model.BreedsModel;
import com.cencosud.breedsapp.mvp.presenter.BreedsPresenter;
import com.cencosud.breedsapp.mvp.view.BreedsFragment;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.junit.Test;

import java.io.IOException;

import static junit.framework.Assert.assertNotNull;

public class BreedsPresenterTest {
    private BreedsFragment view;
    private BreedsPresenter presenter;

    @Test
    public void shouldNotReturnNullModelSpinnerList() throws IOException {
        presenter = new BreedsPresenter(new BreedsFragment(), new BreedsModel());
        JsonObject message = mockCallBreeds();
        assertNotNull(presenter.convertListString(message));
    }

    private JsonObject mockCallBreeds() throws IOException {
        String mockMessage = "{\n" +
                "        \"affenpinscher\": [],\n" +
                "        \"african\": [],\n" +
                "        \"airedale\": [],\n" +
                "        \"akita\": [],\n" +
                "        \"appenzeller\": [],\n" +
                "        \"basenji\": [],\n" +
                "        \"beagle\": [],\n" +
                "        \"bluetick\": [],\n" +
                "        \"borzoi\": [],\n" +
                "        \"bouvier\": [],\n" +
                "        \"boxer\": [],\n" +
                "        \"brabancon\": [],\n" +
                "        \"briard\": [],\n" +
                "        \"bulldog\": [\n" +
                "            \"boston\",\n" +
                "            \"french\"\n" +
                "        ],\n" +
                "        \"bullterrier\": [\n" +
                "            \"staffordshire\"\n" +
                "        ],\n" +
                "        \"cairn\": [],\n" +
                "        \"cattledog\": [\n" +
                "            \"australian\"\n" +
                "        ],\n" +
                "        \"chihuahua\": [],\n" +
                "        \"chow\": [],\n" +
                "        \"clumber\": [],\n" +
                "        \"cockapoo\": [],\n" +
                "        \"collie\": [\n" +
                "            \"border\"\n" +
                "        ],\n" +
                "        \"coonhound\": [],\n" +
                "        \"corgi\": [\n" +
                "            \"cardigan\"\n" +
                "        ],\n" +
                "        \"cotondetulear\": [],\n" +
                "        \"dachshund\": [],\n" +
                "        \"dalmatian\": [],\n" +
                "        \"dane\": [\n" +
                "            \"great\"\n" +
                "        ],\n" +
                "        \"deerhound\": [\n" +
                "            \"scottish\"\n" +
                "        ],\n" +
                "        \"dhole\": [],\n" +
                "        \"dingo\": [],\n" +
                "        \"doberman\": [],\n" +
                "        \"elkhound\": [\n" +
                "            \"norwegian\"\n" +
                "        ],\n" +
                "        \"entlebucher\": [],\n" +
                "        \"eskimo\": [],\n" +
                "        \"germanshepherd\": [],\n" +
                "        \"greyhound\": [\n" +
                "            \"italian\"\n" +
                "        ],\n" +
                "        \"groenendael\": [],\n" +
                "        \"hound\": [\n" +
                "            \"afghan\",\n" +
                "            \"basset\",\n" +
                "            \"blood\",\n" +
                "            \"english\",\n" +
                "            \"ibizan\",\n" +
                "            \"walker\"\n" +
                "        ],\n" +
                "        \"husky\": [],\n" +
                "        \"keeshond\": [],\n" +
                "        \"kelpie\": [],\n" +
                "        \"komondor\": [],\n" +
                "        \"kuvasz\": [],\n" +
                "        \"labrador\": [],\n" +
                "        \"leonberg\": [],\n" +
                "        \"lhasa\": [],\n" +
                "        \"malamute\": [],\n" +
                "        \"malinois\": [],\n" +
                "        \"maltese\": [],\n" +
                "        \"mastiff\": [\n" +
                "            \"bull\",\n" +
                "            \"tibetan\"\n" +
                "        ],\n" +
                "        \"mexicanhairless\": [],\n" +
                "        \"mix\": [],\n" +
                "        \"mountain\": [\n" +
                "            \"bernese\",\n" +
                "            \"swiss\"\n" +
                "        ],\n" +
                "        \"newfoundland\": [],\n" +
                "        \"otterhound\": [],\n" +
                "        \"papillon\": [],\n" +
                "        \"pekinese\": [],\n" +
                "        \"pembroke\": [],\n" +
                "        \"pinscher\": [\n" +
                "            \"miniature\"\n" +
                "        ],\n" +
                "        \"pointer\": [\n" +
                "            \"german\"\n" +
                "        ],\n" +
                "        \"pomeranian\": [],\n" +
                "        \"poodle\": [\n" +
                "            \"miniature\",\n" +
                "            \"standard\",\n" +
                "            \"toy\"\n" +
                "        ],\n" +
                "        \"pug\": [],\n" +
                "        \"puggle\": [],\n" +
                "        \"pyrenees\": [],\n" +
                "        \"redbone\": [],\n" +
                "        \"retriever\": [\n" +
                "            \"chesapeake\",\n" +
                "            \"curly\",\n" +
                "            \"flatcoated\",\n" +
                "            \"golden\"\n" +
                "        ],\n" +
                "        \"ridgeback\": [\n" +
                "            \"rhodesian\"\n" +
                "        ],\n" +
                "        \"rottweiler\": [],\n" +
                "        \"saluki\": [],\n" +
                "        \"samoyed\": [],\n" +
                "        \"schipperke\": [],\n" +
                "        \"schnauzer\": [\n" +
                "            \"giant\",\n" +
                "            \"miniature\"\n" +
                "        ],\n" +
                "        \"setter\": [\n" +
                "            \"english\",\n" +
                "            \"gordon\",\n" +
                "            \"irish\"\n" +
                "        ],\n" +
                "        \"sheepdog\": [\n" +
                "            \"english\",\n" +
                "            \"shetland\"\n" +
                "        ],\n" +
                "        \"shiba\": [],\n" +
                "        \"shihtzu\": [],\n" +
                "        \"spaniel\": [\n" +
                "            \"blenheim\",\n" +
                "            \"brittany\",\n" +
                "            \"cocker\",\n" +
                "            \"irish\",\n" +
                "            \"japanese\",\n" +
                "            \"sussex\",\n" +
                "            \"welsh\"\n" +
                "        ],\n" +
                "        \"springer\": [\n" +
                "            \"english\"\n" +
                "        ],\n" +
                "        \"stbernard\": [],\n" +
                "        \"terrier\": [\n" +
                "            \"american\",\n" +
                "            \"australian\",\n" +
                "            \"bedlington\",\n" +
                "            \"border\",\n" +
                "            \"dandie\",\n" +
                "            \"fox\",\n" +
                "            \"irish\",\n" +
                "            \"kerryblue\",\n" +
                "            \"lakeland\",\n" +
                "            \"norfolk\",\n" +
                "            \"norwich\",\n" +
                "            \"patterdale\",\n" +
                "            \"russell\",\n" +
                "            \"scottish\",\n" +
                "            \"sealyham\",\n" +
                "            \"silky\",\n" +
                "            \"tibetan\",\n" +
                "            \"toy\",\n" +
                "            \"westhighland\",\n" +
                "            \"wheaten\",\n" +
                "            \"yorkshire\"\n" +
                "        ],\n" +
                "        \"vizsla\": [],\n" +
                "        \"weimaraner\": [],\n" +
                "        \"whippet\": [],\n" +
                "        \"wolfhound\": [\n" +
                "            \"irish\"\n" +
                "        ]\n" +
                "    }";
        JsonParser parser = new JsonParser();
        JsonElement jsonTree = parser.parse(mockMessage);
        return jsonTree.getAsJsonObject();
    }
}
