package com.cencosud.breedsapp.schema

import com.google.gson.JsonObject
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Breeds {

    @SerializedName("status")
    @Expose
    val status: String? = null
    @SerializedName("message")
    @Expose
    val message: JsonObject? = null

}
