package com.cencosud.breedsapp.schema

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImageBreed {

    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("message")
    @Expose
    var message: String? = null

}