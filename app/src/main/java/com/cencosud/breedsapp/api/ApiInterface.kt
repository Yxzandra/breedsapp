package com.cencosud.breedsapp.api

import com.cencosud.breedsapp.schema.Breeds
import com.cencosud.breedsapp.schema.ImageBreed
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path


interface ApiInterface {

    @GET("breeds/list/all")
    fun getListBreeds(): Call<Breeds>

    @GET("breed/{nameBreed}/images/random")
    fun getImageBreed(@Path("nameBreed") nameBreed: String): Call<ImageBreed>

}