package com.cencosud.breedsapp.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

abstract class WebService {

    companion object {
        val client = OkHttpClient().newBuilder().build()

        val retrofit = Retrofit.Builder()
                .baseUrl("https://dog.ceo/api/")
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        val retrofitClient = retrofit.create(ApiInterface::class.java)
    }
}
