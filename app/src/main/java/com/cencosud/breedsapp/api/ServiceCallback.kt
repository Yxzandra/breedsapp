package com.cencosud.breedsapp.api

import com.cencosud.breedsapp.schema.ErrorResponse
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

abstract class ServicesCallback<T> : Callback<T> {

    //ServicesCallback permite darle la estructura deseada a las respuesta que vienen de los servicios
    //para mantener una misma estructura para el manejo de errores en el código

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (response.isSuccessful) {
            onSuccess(response.body()!!)
        } else {
            val errorJsonString = response.errorBody()?.string()
            val errorResponse = Gson().fromJson<ErrorResponse>(errorJsonString, ErrorResponse::class.java)
            onError(errorResponse)
        }
    }

    override fun onFailure(call: Call<T>, throwable: Throwable) {
        val error = ErrorResponse()

        error.message = throwable.message.toString()
        error.code = "0"
        onError(error)
    }

    abstract fun onSuccess(response: T)

    abstract fun onError(body: ErrorResponse?)
}