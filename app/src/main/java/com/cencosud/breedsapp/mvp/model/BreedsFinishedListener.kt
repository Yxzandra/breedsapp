package com.cencosud.breedsapp.mvp.model

import com.cencosud.breedsapp.schema.Breeds
import com.cencosud.breedsapp.schema.ErrorResponse
import com.cencosud.breedsapp.schema.ImageBreed

interface BreedsFinishedListener {
    fun onError(body: ErrorResponse)

    fun onSuccessListBreeds(response: Breeds)

    fun onSuccessImageBreed(response: ImageBreed)
}
