package com.cencosud.breedsapp.mvp.presenter


import com.cencosud.breedsapp.mvp.model.BreedsFinishedListener
import com.cencosud.breedsapp.mvp.model.BreedsModel
import com.cencosud.breedsapp.mvp.view.BreedsView
import com.cencosud.breedsapp.schema.Breeds
import com.cencosud.breedsapp.schema.ErrorResponse
import com.cencosud.breedsapp.schema.ImageBreed
import com.google.gson.JsonObject

class BreedsPresenter (val view : BreedsView , val model : BreedsModel) : BreedsFinishedListener {
    override fun onError(body: ErrorResponse) {
        view.showHttpError(body.code!!, body.message!!)
        view.hideProgress()
    }

    override fun onSuccessListBreeds(response: Breeds) {
        val listBreeds = convertListString(response.message)
        view.loadListBreeds(listBreeds)
    }

    fun convertListString(breedsListResponse: JsonObject?): List<String> {
        val list = ArrayList<String>()

        breedsListResponse!!.keySet().forEach { customName ->
            val breed = breedsListResponse[customName].asJsonArray
            if (breed.size() != 0){
                breed.forEach {
                    list.add(it.asString+" "+customName)
                }
            }else{
                list.add(customName)
            }
        }
        return list
    }



    override fun onSuccessImageBreed(response: ImageBreed) {
        view.showImage(response.message!!)
        view.hideProgress()
    }

    fun callListBreeds(){
        view.showProgress()
        model.getAllBreeds(this)
    }

    fun loadImageBreed(nameBreed : String){
        view.showProgress()
        val index = nameBreed.indexOf(' ')
        var result = nameBreed

        if (index != -1) {
            val primary: String? = nameBreed.substring(0, index)
            val second: String? = nameBreed.substring(index+1)
            result = "$second-$primary"
        }

        model.getImageBreed(this,result)
    }
}
