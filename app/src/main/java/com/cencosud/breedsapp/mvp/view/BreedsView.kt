package com.cencosud.breedsapp.mvp.view

interface BreedsView {
    fun showProgress()
    fun hideProgress()
    fun showHttpError(code: String, message: String)
    fun loadListBreeds(breedsList: List<String>)
    fun showImage(image : String)
    fun isLoading(): Boolean
    fun hideImage()
}