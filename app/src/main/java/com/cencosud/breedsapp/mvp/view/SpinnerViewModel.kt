package com.cencosud.breedsapp.mvp.view

import android.arch.lifecycle.ViewModel

class SpinnerViewModel : ViewModel() {
    var modelSpinnerList: List<String>? = null
    var positionSpinnerList: Int = -1
    var imageBreed: String = ""
}