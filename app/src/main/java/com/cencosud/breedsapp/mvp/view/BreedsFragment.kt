package com.cencosud.breedsapp.mvp.view

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.bumptech.glide.Glide
import com.cencosud.breedsapp.R
import com.cencosud.breedsapp.mvp.model.BreedsModel
import com.cencosud.breedsapp.mvp.presenter.BreedsPresenter
import kotlinx.android.synthetic.main.fragment_breeds.*

class BreedsFragment : Fragment(), BreedsView {

    private var mPresenter: BreedsPresenter? = null
    private var viewModel: SpinnerViewModel? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        this.mPresenter = BreedsPresenter(this, BreedsModel())
        this.viewModel = ViewModelProviders.of(this).get(SpinnerViewModel::class.java)

        return inflater.inflate(R.layout.fragment_breeds, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (viewModel!!.modelSpinnerList == null)
            this.mPresenter!!.callListBreeds()
        else{
            showProgress()
            loadListBreeds(viewModel!!.modelSpinnerList!!)
            hideProgress()
        }

    }

    override fun showProgress() {
        this.progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        this.progressBar.visibility = View.INVISIBLE
    }

    override fun isLoading(): Boolean {
        return this.progressBar.visibility == View.VISIBLE
    }

    override fun hideImage() {
        this.breedImage.visibility = View.INVISIBLE
    }

    override fun showHttpError(code: String, message: String) {
        val builder = context?.let {
            AlertDialog.Builder(it)
                    .setTitle("Alert: $code")
                    .setMessage(message)
                    .setPositiveButton("Ok"){
                        dialog, which -> dialog.dismiss()
                    }
        }

        val dialog: AlertDialog = builder!!.create()

        dialog.show()
    }

    override fun loadListBreeds(breedsList: List<String>) {
        this.viewModel!!.modelSpinnerList = breedsList
        //Adapter for spinner
        this.breedSpinner.adapter = ArrayAdapter(context, android.R.layout.simple_spinner_dropdown_item, breedsList)

        //item selected listener for spinner
        this.breedSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, view: View?, position: Int, p3: Long) {
                if (viewModel!!.positionSpinnerList != position) {
                    if (!isLoading()) {
                        showProgress()
                        hideImage()
                    }
                    mPresenter!!.loadImageBreed(breedsList.get(position))
                    viewModel!!.positionSpinnerList = position
                }else{
                    showImage(viewModel!!.imageBreed)
                }

            }

        }

    }

    override fun showImage(image: String) {
        viewModel!!.imageBreed = image
        Glide.with(this)
                .load(image)
                .into(breedImage)
        breedImage.visibility = View.VISIBLE
    }

}