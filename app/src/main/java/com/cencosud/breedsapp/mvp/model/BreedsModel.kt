package com.cencosud.breedsapp.mvp.model

import com.cencosud.breedsapp.api.ServicesCallback
import com.cencosud.breedsapp.api.WebService
import com.cencosud.breedsapp.schema.Breeds
import com.cencosud.breedsapp.schema.ErrorResponse
import com.cencosud.breedsapp.schema.ImageBreed

class BreedsModel{

    fun getAllBreeds(listener: BreedsFinishedListener){
        WebService.retrofitClient.getListBreeds().enqueue(object : ServicesCallback<Breeds>() {

            override fun onError(body: ErrorResponse?) {
                listener.onError(body!!)
            }

            override fun onSuccess(response: Breeds) {
                listener.onSuccessListBreeds(response)
            }
        })
    }

    fun getImageBreed(listener: BreedsFinishedListener,nameBreed : String){
        WebService.retrofitClient.getImageBreed(nameBreed).enqueue(object : ServicesCallback<ImageBreed>() {
            override fun onSuccess(response: ImageBreed) {
                listener.onSuccessImageBreed(response)
            }

            override fun onError(body: ErrorResponse?) {
                listener.onError(body!!)
            }
        })
    }
}