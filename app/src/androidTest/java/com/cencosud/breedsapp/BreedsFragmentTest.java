package com.cencosud.breedsapp;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.support.test.espresso.ViewAction;
import android.support.test.rule.ActivityTestRule;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.cencosud.breedsapp.mvp.view.BreedsFragment;
import com.cencosud.breedsapp.mvp.view.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.matchers.JUnitMatchers.containsString;

public class BreedsFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity> rule =
            new ActivityTestRule<>(MainActivity.class, false, true);


    @Test
    public void shouldKeepSpinnerTextOnScreenRotate(){
        String selectionText = "african";
        onView(withId(R.id.breedSpinner)).perform(click());
        onView(withText(selectionText)).perform(click());
        rule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        onView(withId(R.id.breedSpinner)).check(matches(withSpinnerText(containsString(selectionText))));

    }

    private List<String> mockListCard() {
        JSONArray arr = null;
        try {
            arr = new JSONArray("[\"affenpinscher\", \"african\", \"airedale\", \"akita\", \"appenzeller\", \"basenji\", \"beagle\", \"bluetick\", \"borzoi\", \"bouvier\", \"boxer\", \"brabancon\", \"briard\", \"boston bulldog\", \"french bulldog\", \"staffordshire bullterrier\", \"cairn\", \"australian cattledog\", \"chihuahua\", \"chow\", \"clumber\", \"cockapoo\", \"border collie\", \"coonhound\", \"cardigan corgi\", \"cotondetulear\", \"dachshund\", \"dalmatian\", \"great dane\", \"scottish deerhound\", \"dhole\", \"dingo\", \"doberman\", \"norwegian elkhound\", \"entlebucher\", \"eskimo\", \"germanshepherd\", \"italian greyhound\", \"groenendael\", \"afghan hound\", \"basset hound\", \"blood hound\", \"english hound\", \"ibizan hound\", \"walker hound\", \"husky\", \"keeshond\", \"kelpie\", \"komondor\", \"kuvasz\", \"labrador\", \"leonberg\", \"lhasa\", \"malamute\", \"malinois\", \"maltese\", \"bull mastiff\", \"tibetan mastiff\", \"mexicanhairless\", \"mix\", \"bernese mountain\", \"swiss mountain\", \"newfoundland\", \"otterhound\", \"papillon\", \"pekinese\", \"pembroke\", \"miniature pinscher\", \"german pointer\", \"pomeranian\", \"miniature poodle\", \"standard poodle\", \"toy poodle\", \"pug\", \"puggle\", \"pyrenees\", \"redbone\", \"chesapeake retriever\", \"curly retriever\", \"flatcoated retriever\", \"golden retriever\", \"rhodesian ridgeback\", \"rottweiler\", \"saluki\", \"samoyed\", \"schipperke\", \"giant schnauzer\", \"miniature schnauzer\", \"english setter\", \"gordon setter\", \"irish setter\", \"english sheepdog\", \"shetland sheepdog\", \"shiba\", \"shihtzu\", \"blenheim spaniel\", \"brittany spaniel\", \"cocker spaniel\", \"irish spaniel\", \"japanese spaniel\", \"sussex spaniel\", \"welsh spaniel\", \"english springer\", \"stbernard\", \"american terrier\", \"australian terrier\", \"bedlington terrier\", \"border terrier\", \"dandie terrier\", \"fox terrier\", \"irish terrier\", \"kerryblue terrier\", \"lakeland terrier\", \"norfolk terrier\", \"norwich terrier\", \"patterdale terrier\", \"russell terrier\", \"scottish terrier\", \"sealyham terrier\", \"silky terrier\", \"tibetan terrier\", \"toy terrier\", \"westhighland terrier\", \"wheaten terrier\", \"yorkshire terrier\", \"vizsla\", \"weimaraner\", \"whippet\", \"irish wolfhound\"]\n");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        List<String> list = new ArrayList<>();
        for(int i = 0; i < arr.length(); i++){
            try {
                list.add(arr.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return list;
    }

}
